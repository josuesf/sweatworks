package com.jfl.org.sweatworks.presenter

import android.content.Context
import com.jfl.org.sweatworks.model.Profile
import com.jfl.org.sweatworks.model.ProfileInteractor
import com.jfl.org.sweatworks.model.ProfileInteractorImpl
import com.jfl.org.sweatworks.view.ProfileView

class ProfilePresenterImpl(var profileView: ProfileView) :ProfilePresenter{
    override fun showFavoriteProfiles(profiles: ArrayList<Profile>?) {
        profileView.showFavoriteProfiles(profiles)
    }

    override fun getFavoritesProfiles(context: Context) {
        profileInteractor.getFavoritesProfile(context)
    }

    private var profileInteractor:ProfileInteractor = ProfileInteractorImpl(this)

    override fun showProfiles(profiles: ArrayList<Profile>?) {
       profileView.showProfiles(profiles)
    }

    override fun getProfiles() {
        profileInteractor.getProfilesAPI()
    }

}