package com.jfl.org.sweatworks.view

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.jfl.org.sweatworks.R
import com.jfl.org.sweatworks.model.Profile
import com.squareup.picasso.Picasso

class RecyclerProfileAdapter (var profiles:ArrayList<Profile>?,var resource:Int)
    :RecyclerView.Adapter<RecyclerProfileAdapter.ItemProfileHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ItemProfileHolder {
        var view: View = LayoutInflater.from(p0!!.context).inflate(resource, p0, false)
        return ItemProfileHolder(view)
    }

    override fun getItemCount(): Int {
        return profiles?.size ?:0
    }

    override fun onBindViewHolder(p0: ItemProfileHolder, p1: Int) {
        var profile = profiles?.get(p1)
        p0.setDataCard(profile)
    }

    class ItemProfileHolder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {

        private var profile: Profile? = null
        private var picProfile: ImageView = v.findViewById(R.id.imgProfile)

        init {
            v.setOnClickListener(this)
        }

        fun setDataCard(profile: Profile?){
            this.profile = profile
            Picasso.get().load(profile?.pictureSmall).resize(300, 300).centerCrop().into(picProfile)

        }

        override fun onClick(v: View) {
            Log.i("CLICK Coupon: ", profile?.name)
            val context = v.context
            val showPhotoIntent = Intent(context, ProfileDetailActivity::class.java)
            showPhotoIntent.putExtra("PROFILE", profile)
            context.startActivity(showPhotoIntent)

        }

    }
}