package com.jfl.org.sweatworks.presenter

import android.content.Context
import com.jfl.org.sweatworks.model.Profile

interface ProfilePresenter {
    //View
    fun showProfiles(profiles:ArrayList<Profile>?)
    //Interactor
    fun getProfiles()
    fun getFavoritesProfiles(context: Context)
    fun showFavoriteProfiles(profiles:ArrayList<Profile>?)
}