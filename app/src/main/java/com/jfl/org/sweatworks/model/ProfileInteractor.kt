package com.jfl.org.sweatworks.model

import android.content.Context

interface ProfileInteractor{

    fun getProfilesAPI()
    fun getFavoritesProfile(context: Context)
}