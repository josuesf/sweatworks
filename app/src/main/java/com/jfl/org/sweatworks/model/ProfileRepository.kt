package com.jfl.org.sweatworks.model

import android.content.Context

interface ProfileRepository {
    fun getProfilesAPI()
    fun getFavoriteProfiles(context:Context)
}