package com.jfl.org.sweatworks.view

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.jfl.org.sweatworks.R
import com.jfl.org.sweatworks.model.Profile
import com.squareup.picasso.Picasso
import android.provider.ContactsContract
import android.widget.Toast
import com.jfl.org.sweatworks.model.DbHelper


class ProfileDetailActivity : AppCompatActivity() {
    private var profileSelected: Profile? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_detail)

        profileSelected = intent.getSerializableExtra("PROFILE") as Profile

        var tvName: TextView = findViewById(R.id.tvName)
        var tvEmail: TextView = findViewById(R.id.tvEmail)
        var tvPhone: TextView = findViewById(R.id.tvPhone)
        var ivPicture: ImageView = findViewById(R.id.ivProfile)
        var btnSave: Button = findViewById(R.id.btnSave)
        var btnAddFavorite: Button = findViewById(R.id.btnAddFavorite)

        tvName.text = profileSelected?.name
        tvEmail.text = profileSelected?.email
        tvPhone.text = profileSelected?.phone

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)

        var width = displayMetrics.widthPixels
        Picasso.get().load(profileSelected?.pictureLarge).resize(width, 520).centerCrop().into(ivPicture)

        btnSave.setOnClickListener{
            val intent = Intent(Intent.ACTION_INSERT)
            intent.type = ContactsContract.Contacts.CONTENT_TYPE

            intent.putExtra(ContactsContract.Intents.Insert.NAME, profileSelected?.name)
            intent.putExtra(ContactsContract.Intents.Insert.PHONE, profileSelected?.phone)
            intent.putExtra(ContactsContract.Intents.Insert.EMAIL, profileSelected?.email)

            startActivity(intent)
        }

        btnAddFavorite.setOnClickListener{
            val dbHandler = DbHelper(this, null)
            dbHandler.addProfile(profileSelected!!)
            Toast.makeText(this, profileSelected!!.name + " Added to favorites", Toast.LENGTH_LONG).show()
        }

    }
}