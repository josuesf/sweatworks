package com.jfl.org.sweatworks.model

import android.util.Log
import com.google.gson.JsonObject
import java.io.Serializable
import java.lang.Exception


class Profile(profileJson: JsonObject?) : Serializable {

    lateinit var id: String
    lateinit var email: String
    lateinit var name: String
    lateinit var phone: String
    lateinit var pictureSmall: String
    lateinit var pictureLarge: String

    init {
        try {
            id                  = getId(profileJson!!.get(ID).asJsonObject)
            email               = profileJson!!.get(EMAIL).asString
            name                = getName(profileJson!!.get(NAME).asJsonObject)
            phone               = profileJson!!.get(PHONE).asString
            pictureSmall        = getPictureSize(profileJson!!.get(PICTURE).asJsonObject, SIZE_SMALL)
            pictureLarge        = getPictureSize(profileJson!!.get(PICTURE).asJsonObject, SIZE_LARGE)
        }catch (e: Exception){
            //e.printStackTrace()
        }


    }

    private fun getId(id: JsonObject?): String {
        return id!!.get("uuid").asString ?: ""
    }

    private fun getPictureSize(picture: JsonObject?, size: String): String {
        return picture!!.get(size).asString
    }

    private fun getName(name: JsonObject?): String {
        val title = name!!.get("title").asString
        val first = name!!.get("first").asString
        val last = name!!.get("last").asString
        return "$title $first $last"
    }

    companion object {
        private val ID                  = "login"
        private val EMAIL               = "email"
        private val NAME                = "name"
        private val PHONE               = "phone"
        private val PICTURE             = "picture"
        private val SIZE_SMALL          = "medium"
        private val SIZE_LARGE          = "large"
    }
}