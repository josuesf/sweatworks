package com.jfl.org.sweatworks.model

import android.content.Context
import android.util.Log
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.jfl.org.sweatworks.presenter.ProfilePresenter
import com.jfl.org.sweatworks.view.MainActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileRepositoryImpl(var profilePresenter: ProfilePresenter) :ProfileRepository {


    override fun getProfilesAPI() {
        //CONTROLLER
        var profiles: ArrayList<Profile>? = ArrayList<Profile>()
        val apiAdapter = ApiAdapter()
        val apiService = apiAdapter.getClientService()
        val call = apiService.getProfiles()

        call.enqueue(object : Callback<JsonObject> {
            override fun onFailure(call: Call<JsonObject>, t: Throwable) {
                Log.e("ERROR: ", t.message)
                t.stackTrace
            }

            override fun onResponse(call: Call<JsonObject>, response: Response<JsonObject>) {
                val offersJsonArray = response.body()?.getAsJsonArray("results")
                offersJsonArray?.forEach { jsonElement: JsonElement ->
                    var jsonObject = jsonElement.asJsonObject
                    var profile = Profile(jsonObject)
                    profiles?.add(profile)
                }
                //VIEW
                profilePresenter.showProfiles(profiles)
                //VIEW
            }
        })
    }

    override fun getFavoriteProfiles(context:Context) {
        val dbHandler = DbHelper(context, null)
        profilePresenter.showFavoriteProfiles(dbHandler.getAllProfiles())
    }
}