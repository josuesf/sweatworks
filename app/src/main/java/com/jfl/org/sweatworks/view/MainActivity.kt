package com.jfl.org.sweatworks.view

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.jfl.org.sweatworks.R
import com.jfl.org.sweatworks.model.Profile
import com.jfl.org.sweatworks.presenter.ProfilePresenter
import com.jfl.org.sweatworks.presenter.ProfilePresenterImpl

class MainActivity : AppCompatActivity(),ProfileView {

    private var profilePresenter: ProfilePresenter? = null
    private var rvProfiles: RecyclerView? = null
    private var rvProfilesFav: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        profilePresenter = ProfilePresenterImpl(this)
        //VIEW
        rvProfiles = findViewById(R.id.rvProfiles)
        rvProfiles?.layoutManager = GridLayoutManager(this,3)

        rvProfilesFav = findViewById(R.id.rvFavorites)
        rvProfilesFav?.layoutManager = LinearLayoutManager(this,0,false)

        //VIEW
        getProfiles()
        getFavoriteProfile(this)
    }

    override fun getProfiles() {
        try {
            profilePresenter?.getProfiles()
        }catch (e: Exception){
            e.printStackTrace()
        }

    }

    override fun showProfiles(profiles: ArrayList<Profile>?) {
        rvProfiles!!.adapter = RecyclerProfileAdapter(profiles,R.layout.profile_item)
    }

    override fun getFavoriteProfile(context: Context) {
        try {
            profilePresenter?.getFavoritesProfiles(context)
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
    override fun showFavoriteProfiles(profiles: ArrayList<Profile>?) {
        rvProfilesFav!!.adapter = RecyclerProfileAdapter(profiles,R.layout.profile_favorite)
    }

    override fun onPostResume() {
        super.onPostResume()
        getFavoriteProfile(this)
    }
}
