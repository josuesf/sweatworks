package com.jfl.org.sweatworks.view

import android.content.Context
import com.jfl.org.sweatworks.model.Profile

interface ProfileView {
    fun getProfiles()
    fun showProfiles(profiles: ArrayList<Profile>?)
    fun getFavoriteProfile(context: Context)
    fun showFavoriteProfiles(profiles: ArrayList<Profile>?)
}