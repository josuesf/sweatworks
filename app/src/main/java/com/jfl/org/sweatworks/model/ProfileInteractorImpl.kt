package com.jfl.org.sweatworks.model

import android.content.Context
import com.jfl.org.sweatworks.presenter.ProfilePresenter

class ProfileInteractorImpl(profilePresenter: ProfilePresenter) :ProfileInteractor {


    private var profileRepository:ProfileRepository = ProfileRepositoryImpl(profilePresenter)
    override fun getProfilesAPI() {
        profileRepository.getProfilesAPI()
    }
    override fun getFavoritesProfile(context: Context) {
        profileRepository.getFavoriteProfiles(context)
    }
}