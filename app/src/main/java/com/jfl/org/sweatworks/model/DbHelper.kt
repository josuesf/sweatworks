package com.jfl.org.sweatworks.model

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.google.gson.JsonObject

class DbHelper(context: Context,
               factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME,
        factory, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_PRODUCTS_TABLE = ("CREATE TABLE " +
                TABLE_NAME + "("
                + COLUMN_ID + " TEXT PRIMARY KEY," +
                COLUMN_NAME+ " TEXT," +
                COLUMN_EMAIL+ " TEXT," +
                COLUMN_PHONE+ " TEXT," +
                COLUMN_IMG_SMALL+ " TEXT," +
                COLUMN_IMG_LARGE+ " TEXT"
                + ")")
        db.execSQL(CREATE_PRODUCTS_TABLE)
    }
    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }
    fun addProfile(profile: Profile) {
        val values = ContentValues()
        values.put(COLUMN_ID, profile.id)
        values.put(COLUMN_NAME, profile.name)
        values.put(COLUMN_EMAIL, profile.email)
        values.put(COLUMN_PHONE, profile.phone)
        values.put(COLUMN_IMG_SMALL, profile.pictureSmall)
        values.put(COLUMN_IMG_LARGE, profile.pictureLarge)
        val db = this.writableDatabase
        db.insert(TABLE_NAME, null, values)
        db.close()
    }
    fun getAllProfiles(): ArrayList<Profile>? {
        val db = this.readableDatabase
        val cursor = db.rawQuery("SELECT * FROM $TABLE_NAME", null)
        val profiles:ArrayList<Profile> = ArrayList()
        cursor!!.moveToFirst()
        while (cursor.moveToNext()) {
            val profile = Profile(null)
            profile.id = cursor.getString(cursor.getColumnIndex(COLUMN_ID))
            profile.name = cursor.getString(cursor.getColumnIndex(COLUMN_NAME))
            profile.email = cursor.getString(cursor.getColumnIndex(COLUMN_ID))
            profile.phone = cursor.getString(cursor.getColumnIndex(COLUMN_PHONE))
            profile.pictureSmall = cursor.getString(cursor.getColumnIndex(COLUMN_IMG_SMALL))
            profile.pictureLarge = cursor.getString(cursor.getColumnIndex(COLUMN_IMG_LARGE))
            profiles.add(profile)
        }
        cursor.close()
        return profiles
    }
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "sweatworks.db"
        val TABLE_NAME = "profiles"
        val COLUMN_ID = "id"
        val COLUMN_NAME = "name"
        val COLUMN_EMAIL = "email"
        val COLUMN_PHONE = "phone"
        val COLUMN_IMG_SMALL = "pictureSmall"
        val COLUMN_IMG_LARGE = "pictureLarge"

    }
}