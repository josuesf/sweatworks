package com.jfl.org.sweatworks.model

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET

interface ApiService {
    @GET("api/?results=50")
    fun getProfiles(): Call<JsonObject>
}